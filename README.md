# Colorizer Classes

Adds in the ability to take a color value from Drupal and output it as a CSS Class using a Twig filter. It was built to be used with something like the Color Field module that lets users see a visual color in the admin interface but still output a CSS class in the markup.