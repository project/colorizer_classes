<?php

namespace Drupal\colorizer_classes;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * Twig extension.
 */
class ColorizerClassesTwigExtension extends AbstractExtension {

  /**
   * {@inheritdoc}
   */
  public function getFilters() {
    return [
      new TwigFilter('colorizer', function ($text) {
        $color_mapping_settings = \Drupal::config('colorizer_classes.settings')->get('color_mapping');

        if (is_string($text) && $color_mapping_settings !== NULL) {
          $text = str_replace("\n", '', str_replace(' ', '', $text));
          $color_mapping = explode(PHP_EOL, $color_mapping_settings);
          foreach ($color_mapping as $color) {
            $color = explode('|', $color);
            if (strtoupper($color[0]) === strtoupper($text)) {
              return $color[1];
            }
          }
        }
        return $text;
      }),
    ];
  }

}
