<?php

namespace Drupal\colorizer_classes\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Colorizer Classes settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'colorizer_classes_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['colorizer_classes.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $color_mapping = $this->config('colorizer_classes.settings')->get('color_mapping');
    $form['color_mapping'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Color Classes Mapping'),
      '#description' => $this->t('The colors coming from Drupal to the css classes you want to map them to in KEY|VALUE form.'),
      '#default_value' => $color_mapping ?? "#000000|color-black\n#FFFFFF|color-white",
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('colorizer_classes.settings')
      ->set('color_mapping', $form_state->getValue('color_mapping'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
